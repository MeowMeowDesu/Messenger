package main

import (
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/limiter"
	"gitlab.com/MeowMeowDesu/Messenger/internal/routing"
)

func main() {
	app := fiber.New(fiber.Config{
		BodyLimit:    64 * 1024 * 1024,
		ServerHeader: "Baka-Messenger",
		AppName:      "Baka-Messenger",
	})
	app.Server().MaxConnsPerIP = 4
	app.Server().Name = "Baka-Messenger"
	app.Use(limiter.New(limiter.Config{
		Max:        20,
		Expiration: 30 * time.Second,
	}))
	app.Server().StreamRequestBody = true

	app.Static("/static", "./files")
	app.Get("/", routing.Index)
	//app.Get("/swagger/*", fiberSwagger.WrapHandler)
	app.Get("/GetIp", routing.GetIp)
}
