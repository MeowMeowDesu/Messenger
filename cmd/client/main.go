package main

import (
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/limiter"
	"gitlab.com/MeowMeowDesu/Messenger/internal/routing"
)

func main() {
	app := fiber.New(fiber.Config{
		BodyLimit:    64 * 1024 * 1024,
		ServerHeader: "Baka-Messenger-Client",
		AppName:      "Baka-Messenger-Client",
	})
	app.Server().MaxConnsPerIP = 4
	app.Server().Name = "Baka-Messenger-Client"
	app.Use(limiter.New(limiter.Config{
		Max:        2,
		Expiration: 30 * time.Second,
	}))

	app.Get("/", routing.Index)
}
