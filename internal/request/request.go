package request

import (
	"log"
	"net/http"
)

func Request(host, method, token string) (resp *http.Response) {
	client := http.Client{}
	req, reqErr := http.NewRequest(method, "https://"+host, nil)
	if reqErr != nil {
		log.Println(reqErr)
		return nil
	}

	req.Header.Add("Authorization", token)

	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return nil
	}
	return resp
}
