package configs

import (
	"log"
	"os"
	"path/filepath"
	"strings"

	"gopkg.in/yaml.v2"
)

func init() {
	pwd, err := os.Getwd()
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}

	split := strings.Split(pwd, string(os.PathSeparator))
	if split[len(split)-1] == "CDN" {
		WorkDirPath = pwd
	} else {
		log.Println(split[len(split)-1])
		pwd = filepath.Dir(pwd)
		pwd = filepath.Dir(pwd)
		WorkDirPath = pwd
	}
	absPath := filepath.Join(WorkDirPath, "configs", "settings.yml")
	err = Config.GetConfig(absPath)
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
}

func (c *config) GetConfig(configPath string) error {
	file, err := os.Open(configPath)
	if err != nil {
		return err
	}

	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(file)

	d := yaml.NewDecoder(file)

	if err := d.Decode(&c); err != nil {
		return err
	}
	return nil
}
