package configs

var (
	Config      config
	WorkDirPath string
)

type config struct {
	Rest struct {
		ApiToken string `yaml:"tokenapi"`
	}
	Psql struct {
		Password string `yaml:"password"`
	}
}
